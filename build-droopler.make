core = 8.x
api = 2
includes[] = drupal-org-core.make
includes[] = drupal-org.make
projects[droopler][type] = profile
projects[droopler][download][type] = git
projects[droopler][download][branch] = 8.x-6.x
