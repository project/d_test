core: 8.x
api: 2
defaults:
  projects:
    subdir: contrib
projects:
  admin_toolbar:
    version: '1.27'
  better_exposed_filters:
    version: 3.0-alpha6
  checklistapi:
    version: '1.10'
  colorbox:
    version: '1.4'
  config_update:
    version: '1.6'
  contact_formatter:
    version: '1.1'
  ctools:
    version: '3.2'
  entity_reference_revisions:
    version: '1.6'
  facets:
    version: '1.4'
  features:
    version: '3.8'
  field_group:
    version: 3.0-rc1
  geysir:
    version: '1.1'
  link_attributes:
    version: '1.7'
  linkit:
    version: '4.3'
  menu_link_attributes:
    version: '1.0'
  metatag:
    version: '1.8'
  paragraphs:
    version: '1.8'
  pathauto:
    version: '1.4'
  redirect:
    version: '1.3'
  search_api:
    version: '1.13'
  simple_sitemap:
    version: '2.12'
  smtp:
    version: 1.0-beta4
  svg_image:
    version: '1.9'
  token:
    version: '1.5'
  tvi:
    version: 1.0-beta2